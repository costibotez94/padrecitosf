<?php
/**
 * The template for displaying page home
 *
 * @author Botez Costin
 * @package Padrecito
 */

get_header(); ?>

	<div class="position_content" id="page_position_content">
		<div class="clearfix colelem" id="pu87-3"><!-- group -->
     		<!-- m_editable region-id="editable-static-tag-U87-BP_infinity" template="index.html" data-type="html" data-ice-options="disableImageResize,link" -->
     		<div class="clearfix grpelem" id="u87-3" data-muse-uid="U87" data-muse-type="txt_frame"><!-- content -->
	      		<p>&nbsp;</p>
	     	</div>
     		<!-- /m_editable -->
     		<!-- m_editable region-id="editable-static-tag-U389-BP_infinity" template="index.html" data-type="image" -->
     		<div class="clip_frame clearfix grpelem" id="u389" data-muse-uid="U389" data-muse-type="img_frame"><!-- image -->
      			<img class="position_content" id="u389_img" src="<?php echo get_template_directory_uri(); ?>/images/padrecito_logo_blue.png" alt="" width="546" height="195" data-muse-src="<?php echo get_template_directory_uri(); ?>/images/padrecito_logo_blue.png"/>
     		</div>
     		<!-- /m_editable -->
     		<a class="nonblock nontext grpelem" id="u538-9" href="<?php echo home_url('contact'); ?>" data-href="page:U770" data-mu-ie-matrix="progid:DXImageTransform.Microsoft.Matrix(M11=0.9994,M12=-0.0349,M21=0.0349,M22=0.9994,SizingMethod='auto expand')" data-mu-ie-matrix-dx="-2" data-mu-ie-matrix-dy="-4"><!-- rasterized frame --><img id="u538-9_img" alt="901 cole st. @ Carl San Francisco, CA 94117 415.742.5505" width="241" height="92" src="<?php echo get_template_directory_uri(); ?>/images/u538-9.png"/></a>
     		<div class="grpelem" id="u1433-18" data-mu-ie-matrix="progid:DXImageTransform.Microsoft.Matrix(M11=0.9998,M12=0.0175,M21=-0.0175,M22=0.9998,SizingMethod='auto expand')" data-mu-ie-matrix-dx="-1" data-mu-ie-matrix-dy="-2"><!-- rasterized frame -->
      			<img id="u1433-18_img" alt="DINNER 5:30 - 10:00 mon - thu 5:00 - 10:30 Fri- Sat 5:00 - 10:00 sun " width="236" height="127" src="<?php echo get_template_directory_uri(); ?>/images/u1433-18.png"/>
     		</div>
     		<div class="grpelem" id="u1734-10" data-mu-ie-matrix="progid:DXImageTransform.Microsoft.Matrix(M11=0.9998,M12=-0.0175,M21=0.0175,M22=0.9998,SizingMethod='auto expand')" data-mu-ie-matrix-dx="-1" data-mu-ie-matrix-dy="-2"><!-- rasterized frame -->
      			<img id="u1734-10_img" alt="BRUNCH 10AM - 2:15PM SAT-SUN " width="253" height="127" src="<?php echo get_template_directory_uri(); ?>/images/u1734-10.png"/>
     		</div>
     		<!-- m_editable region-id="editable-static-tag-U1738-BP_infinity" template="index.html" data-type="image" -->
     		<div class="clip_frame grpelem" id="u1738" data-muse-uid="U1738" data-muse-type="img_frame"><!-- image -->
      			<img class="block" id="u1738_img" src="<?php echo get_template_directory_uri(); ?>/images/padrecitoborderxtralong-crop-u1738.png" alt="" width="947" height="18" data-muse-src="<?php echo get_template_directory_uri(); ?>/images/padrecitoborderxtralong-crop-u1738.png"/>
     		</div>
     		<!-- /m_editable -->
    	</div>
    	<div class="clearfix colelem" id="ppu129"><!-- group -->
     		<a class="nonblock nontext grpelem" id="u129" href="<?php echo home_url('menu'); ?>" data-href="page:U762" data-mu-ie-matrix="progid:DXImageTransform.Microsoft.Matrix(M11=0.9998,M12=-0.0175,M21=0.0175,M22=0.9998,SizingMethod='auto expand')" data-mu-ie-matrix-dx="0" data-mu-ie-matrix-dy="-1"><!-- state-based BG <?php echo get_template_directory_uri(); ?>/inc/images --><img id="u129_states" alt="MENU" src="<?php echo get_template_directory_uri(); ?>/images/blank.gif"/></a>
     		<a class="nonblock nontext grpelem" id="u132" href="<?php echo home_url('about'); ?>" data-href="page:U738" data-mu-ie-matrix="progid:DXImageTransform.Microsoft.Matrix(M11=0.9998,M12=0.0175,M21=-0.0175,M22=0.9998,SizingMethod='auto expand')" data-mu-ie-matrix-dx="0" data-mu-ie-matrix-dy="-1"><!-- state-based BG <?php echo get_template_directory_uri(); ?>/inc/images --><img id="u132_states" alt="about" src="<?php echo get_template_directory_uri(); ?>/images/blank.gif"/></a>
     		<a class="nonblock nontext grpelem" id="u134" href="<?php echo home_url('contact'); ?>" data-href="page:U770"><!-- state-based BG <?php echo get_template_directory_uri(); ?>/inc/images --><img id="u134_states" alt="Contact" src="<?php echo get_template_directory_uri(); ?>/images/blank.gif"/></a>
     		<!-- m_editable region-id="editable-static-tag-U183-BP_infinity" template="index.html" data-type="image" -->
     		<div class="clip_frame grpelem" id="u183" data-muse-uid="U183" data-muse-type="img_frame"><!-- image -->
      			<img class="block" id="u183_img" src="<?php echo get_template_directory_uri(); ?>/images/paintedbackground.png" alt="" width="687" height="481" data-muse-src="<?php echo get_template_directory_uri(); ?>/images/paintedbackground.png"/>
     		</div>
     		<!-- /m_editable -->
     		<a class="nonblock nontext grpelem" id="u136" href="<?php echo home_url('reservations'); ?>" data-href="page:U746" data-mu-ie-matrix="progid:DXImageTransform.Microsoft.Matrix(M11=0.9998,M12=-0.0175,M21=0.0175,M22=0.9998,SizingMethod='auto expand')" data-mu-ie-matrix-dx="0" data-mu-ie-matrix-dy="-2"><!-- state-based BG <?php //echo get_template_directory_uri(); ?>/inc/images --><img id="u136_states" alt="reservations" src="<?php echo get_template_directory_uri(); ?>/images/blank.gif"/></a>
     		<a class="nonblock nontext grpelem" id="u139" href="https://www.treatful.com/buy/Padrecito?ref=1" data-mu-ie-matrix="progid:DXImageTransform.Microsoft.Matrix(M11=0.9998,M12=0.0175,M21=-0.0175,M22=0.9998,SizingMethod='auto expand')" data-mu-ie-matrix-dx="-1" data-mu-ie-matrix-dy="-2"><!-- state-based BG <?php //echo get_template_directory_uri(); ?>/inc/images --><img id="u139_states" alt="gift certificates" src="<?php echo get_template_directory_uri(); ?>/images/blank.gif"/></a>
     		<!-- m_editable region-id="editable-static-tag-U150-BP_infinity" template="index.html" data-type="image" -->
     		<div class="clip_frame grpelem" id="u150" data-muse-uid="U150" data-muse-type="img_frame"><!-- image -->
      			<img class="block" id="u150_img" src="<?php echo get_template_directory_uri(); ?>/images/diablo.png" alt="" width="191" height="238" data-muse-src="<?php echo get_template_directory_uri(); ?>/images/diablo.png"/>
     		</div>
     		<!-- /m_editable -->
     		<!-- m_editable region-id="editable-static-tag-U155-BP_infinity" template="index.html" data-type="image" -->
     		<div class="clip_frame grpelem" id="u155" data-muse-uid="U155" data-muse-type="img_frame"><!-- image -->
      			<img class="block" id="u155_img" src="<?php echo get_template_directory_uri(); ?>/images/ladama.png" alt="" width="205" height="248" data-muse-src="<?php echo get_template_directory_uri(); ?>/images/ladama.png"/>
     		</div>
     		<!-- /m_editable -->
     		<!-- m_editable region-id="editable-static-tag-U1791-BP_infinity" template="index.html" data-type="image" -->
     		<div class="clip_frame grpelem" id="u1791" data-muse-uid="U1791" data-muse-type="img_frame"><!-- image -->
      			<img class="block" id="u1791_img" src="<?php echo get_template_directory_uri(); ?>/images/padrepic1.jpg" alt="" width="474" height="316" data-muse-src="<?php echo get_template_directory_uri(); ?>/images/padrepic1.jpg"/>
     		</div>
     		<!-- /m_editable -->
     		<div class="clearfix grpelem" id="u1797"><!-- group -->
      			<div class="grpelem" id="u1736" data-mu-ie-matrix="progid:DXImageTransform.Microsoft.Matrix(M11=0.9998,M12=-0.0175,M21=0.0175,M22=0.9998,SizingMethod='auto expand')" data-mu-ie-matrix-dx="-3" data-mu-ie-matrix-dy="-3"><!-- simple frame --></div>
      			<div class="grpelem" id="u1737" data-mu-ie-matrix="progid:DXImageTransform.Microsoft.Matrix(M11=0.9998,M12=-0.0175,M21=0.0175,M22=0.9998,SizingMethod='auto expand')" data-mu-ie-matrix-dx="-3" data-mu-ie-matrix-dy="-2"><!-- simple frame --></div>
      			<a class="nonblock nontext grpelem" id="u1735-23" href="<?php echo home_url('contact'); ?>" data-href="page:U770" data-mu-ie-matrix="progid:DXImageTransform.Microsoft.Matrix(M11=0.9998,M12=-0.0175,M21=0.0175,M22=0.9998,SizingMethod='auto expand')" data-mu-ie-matrix-dx="-3" data-mu-ie-matrix-dy="-2"><!-- rasterized frame --><img id="u1735-23_img" alt="LA HORA FELIZ (HAPPY HOUR) Mon-THU 5:30 TO 6:30 Friday 5 to 6 " width="253" height="364" src="<?php echo get_template_directory_uri(); ?>/images/u1735-23.png"/></a>
      			<!-- m_editable region-id="editable-static-tag-U598-BP_infinity" template="index.html" data-type="image" -->
      			<div class="clip_frame grpelem" id="u598" data-muse-uid="U598" data-muse-type="img_frame" data-mu-ie-matrix="progid:DXImageTransform.Microsoft.Matrix(M11=0.9998,M12=-0.0175,M21=0.0175,M22=0.9998,SizingMethod='auto expand')" data-mu-ie-matrix-dx="0" data-mu-ie-matrix-dy="-2"><!-- image -->
       				<img class="block" id="u598_img" src="<?php echo get_template_directory_uri(); ?>/images/padrecitoborderxtralong-crop-u598.png" alt="" width="241" height="18" data-muse-src="<?php echo get_template_directory_uri(); ?>/images/padrecitoborderxtralong-crop-u598.png"/>
      			</div>
      			<!-- /m_editable -->
     		</div>
     		<!-- m_editable region-id="editable-static-tag-U1741" template="index.html" data-type="html" data-ice-options="clickable" data-ice-editable="link" -->
     		<a class="nonblock nontext grpelem" id="u1741" href="<?php echo get_template_directory_uri(); ?>/inc/assets/padre.happyhour.03.28.14.pdf" data-href="upload:U1759" data-muse-uid="U1741"><!-- simple frame --></a>
     		<!-- /m_editable -->
    	</div>
    	<div class="verticalspacer" data-offset-top="901" data-content-above-spacer="956" data-content-below-spacer="62"></div>
    </div><!-- #page_position_content -->
<?php
get_footer();
