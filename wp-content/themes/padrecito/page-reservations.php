<?php
/**
 * The template for displaying page reservations
 *
 * @author Botez Costin
 * @package Padrecito
 */

get_header(); ?>

	<div class="position_content" id="page_position_content">
    	<div class="clearfix colelem" id="pu843-4"><!-- group -->
     		<img class="grpelem" id="u843-4" alt="reservations" width="235" height="38" src="<?php echo get_template_directory_uri(); ?>/images/u843-4.png"/><!-- rasterized frame -->
     		<!-- m_editable region-id="editable-static-tag-U849-BP_infinity" template="reservations-at-padrecito.html" data-type="image" -->
     		<div class="clip_frame clearfix grpelem" id="u849" data-muse-uid="U849" data-muse-type="img_frame"><!-- image -->
      			<img class="position_content" id="u849_img" src="<?php echo get_template_directory_uri(); ?>/images/padrecitoborderxtralong.png" alt="" width="1215" height="18" data-muse-src="<?php echo get_template_directory_uri(); ?>/images/padrecitoborderxtralong.png"/>
     		</div>
     		<!-- /m_editable -->
     		<!-- m_editable region-id="editable-static-tag-U900-BP_infinity" template="reservations-at-padrecito.html" data-type="image" data-ice-options="clickable" data-ice-editable="link" -->
     		<a class="nonblock nontext clip_frame grpelem" id="u900" href="index.html" data-href="page:U65" data-muse-uid="U900" data-muse-type="img_frame"><!-- image --><img class="block" id="u900_img" src="<?php echo get_template_directory_uri(); ?>/images/padrecito_logo_blue.png" alt="" width="199" height="71" data-muse-src="<?php echo get_template_directory_uri(); ?>/images/padrecito_logo_blue.png"/></a>
     		<!-- /m_editable -->
    	</div>
    	<div class="clearfix colelem" id="pu832"><!-- group -->
     		<!-- m_editable region-id="editable-static-tag-U832-BP_infinity" template="reservations-at-padrecito.html" data-type="image" -->
     		<div class="clip_frame grpelem" id="u832" data-muse-uid="U832" data-muse-type="img_frame"><!-- image -->
      			<img class="block" id="u832_img" src="<?php echo get_template_directory_uri(); ?>/images/paintedbackground.png" alt="" width="747" height="523" data-muse-src="<?php echo get_template_directory_uri(); ?>/images/paintedbackground.png"/>
     		</div>
     		<!-- /m_editable -->
     		<!-- m_editable region-id="editable-static-tag-U841-BP_infinity" template="reservations-at-padrecito.html" data-type="image" -->
     		<div class="clip_frame grpelem" id="u841" data-muse-uid="U841" data-muse-type="img_frame"><!-- image -->
      			<img class="block" id="u841_img" src="<?php echo get_template_directory_uri(); ?>/images/placeholder2.jpg" alt="" width="462" height="323" data-muse-src="<?php echo get_template_directory_uri(); ?>/images/placeholder2.jpg"/>
     		</div>
     		<!-- /m_editable -->
     		<!-- m_editable region-id="editable-static-tag-U844-BP_infinity" template="reservations-at-padrecito.html" data-type="html" data-ice-options="disableImageResize,link" -->
     		<div class="clearfix grpelem" id="u844-12" data-muse-uid="U844" data-muse-type="txt_frame"><!-- content -->
     			<?php global $post; echo $post->post_content; ?>
     		</div>
     		<!-- /m_editable -->
     		<!-- m_editable region-id="editable-static-tag-U845-BP_infinity" template="reservations-at-padrecito.html" data-type="image" -->
     		<div class="clip_frame grpelem" id="u845" data-muse-uid="U845" data-muse-type="img_frame"><!-- image -->
      			<img class="block" id="u845_img" src="<?php echo get_template_directory_uri(); ?>/images/ladama.png" alt="" width="205" height="248" data-muse-src="<?php echo get_template_directory_uri(); ?>/images/ladama.png"/>
     		</div>
     		<!-- /m_editable -->
     		<!-- m_editable region-id="editable-static-tag-U847-BP_infinity" template="reservations-at-padrecito.html" data-type="image" -->
     		<div class="clip_frame grpelem" id="u847" data-muse-uid="U847" data-muse-type="img_frame"><!-- image -->
      			<img class="block" id="u847_img" src="<?php echo get_template_directory_uri(); ?>/images/diablo.png" alt="" width="191" height="238" data-muse-src="<?php echo get_template_directory_uri(); ?>/images/diablo.png"/>
     		</div>
     		<!-- /m_editable -->
     		<div class="grpelem" id="u839"><!-- custom html -->
      			<script type="text/javascript" src="http://www.opentable.com/frontdoor/default.aspx?rid=103708&restref=103708&bgcolor=F6F6F3&titlecolor=0F0F0F&subtitlecolor=794D0A&btnbgimage=http://www.opentable.com/frontdoor/img/ot_btn_red.png&otlink=FFFFFF&icon=dark&mode=wide&hover=1"></script><a href="http://www.opentable.com/padrecito-reservations-san-francisco?rtype=ism&restref=103708" class="OT_ExtLink">Padrecito Reservations</a>
     		</div>
    	</div>
    	<div class="clearfix colelem" id="ppu834"><!-- group -->
     		<a class="nonblock nontext grpelem" id="u834" href="<?php echo home_url(); ?>" data-href="page:U65"><!-- state-based BG images --><img id="u834_states" alt="home" src="<?php echo get_template_directory_uri(); ?>/images/blank.gif"/></a>
     		<a class="nonblock nontext grpelem" id="u835" href="<?php echo home_url('menu'); ?>" data-href="page:U762"><!-- state-based BG images --><img id="u835_states" alt="menu" src="<?php echo get_template_directory_uri(); ?>/images/blank.gif"/></a>
     		<a class="nonblock nontext grpelem" id="u836" href="<?php echo home_url('contact'); ?>" data-href="page:U770"><!-- state-based BG images --><img id="u836_states" alt="Contact" src="<?php echo get_template_directory_uri(); ?>/images/blank.gif"/></a>
     		<a class="nonblock nontext grpelem" id="u837" href="<?php echo home_url('about'); ?>" data-href="page:U738"><!-- state-based BG images --><img id="u837_states" alt="about" src="<?php echo get_template_directory_uri(); ?>/images/blank.gif"/></a>
     		<a class="nonblock nontext grpelem" id="u838" href="https://www.treatful.com/buy/Padrecito?ref=1"><!-- state-based BG images --><img id="u838_states" alt="gift certificates" src="<?php echo get_template_directory_uri(); ?>/images/blank.gif"/></a>
    	</div>
    	<div class="verticalspacer" data-offset-top="723" data-content-above-spacer="758" data-content-below-spacer="98"></div>
   	</div>
<?php
get_footer();
