<?php
/**
 * The template for displaying page contact
 *
 * @author Botez Costin
 * @package Padrecito
 */

get_header();?>
	<div class="clearfix colelem" id="pu809-4"><!-- group -->
    	<img class="grpelem" id="u809-4" alt="contact" width="185" height="38" src="<?php echo get_template_directory_uri(); ?>/images/u809-4.png"/><!-- rasterized frame -->
	    <!-- m_editable region-id="editable-static-tag-U825-BP_infinity" template="contact-padrecito.html" data-type="image" -->
	    <div class="clip_frame grpelem" id="u825" data-muse-uid="U825" data-muse-type="img_frame"><!-- image -->
	    	<img class="block" id="u825_img" src="<?php echo get_template_directory_uri(); ?>/images/padrecitoborderxtralong-crop-u825.png" alt="" width="771" height="18" data-muse-src="<?php echo get_template_directory_uri(); ?>/images/padrecitoborderxtralong-crop-u825.png"/>
	    </div>
    	<!-- /m_editable -->
    	<!-- m_editable region-id="editable-static-tag-U906-BP_infinity" template="contact-padrecito.html" data-type="image" data-ice-options="clickable" data-ice-editable="link" -->
    	<a class="nonblock nontext clip_frame grpelem" id="u906" href="<?php echo admin_url(); ?>" data-href="page:U65" data-muse-uid="U906" data-muse-type="img_frame"><!-- image --><img class="block" id="u906_img" src="<?php echo get_template_directory_uri(); ?>/images/padrecito_logo_blue.png" alt="" width="199" height="71" data-muse-src="<?php echo get_template_directory_uri(); ?>/images/padrecito_logo_blue.png"/></a>
    	<!-- /m_editable -->
   	</div>
   	<div class="clearfix colelem" id="pu1436-4"><!-- group -->
    	<img class="grpelem" id="u1436-4" alt="email" width="185" height="38" src="<?php echo get_template_directory_uri(); ?>/images/u1436-4.png?crc=3842383479"/><!-- rasterized frame -->
    	<!-- m_editable region-id="editable-static-tag-U810-BP_infinity" template="contact-padrecito.html" data-type="html" data-ice-options="disableImageResize,link" -->
    	<div class="clearfix grpelem" id="u810-10" data-muse-uid="U810" data-muse-type="txt_frame"><!-- content -->
     		<?php global $post; echo $post->post_content; ?>
    	</div>
	    <!-- /m_editable -->
	    <!-- m_editable region-id="editable-static-tag-U815-BP_infinity" template="contact-padrecito.html" data-type="image" -->
	    <div class="clip_frame grpelem" id="u815" data-muse-uid="U815" data-muse-type="img_frame"><!-- image -->
	     	<img class="block" id="u815_img" src="<?php echo get_template_directory_uri(); ?>/images/paintedbackground.png" alt="" width="764" height="535" data-muse-src="<?php echo get_template_directory_uri(); ?>/images/paintedbackground.png"/>
	    </div>
	    <!-- /m_editable -->
	    <div class="grpelem" id="u819"><!-- custom html -->
	     	<iframe width="554" height="368" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=901+Cole+Street,+San+Francisco,+CA&amp;aq=0&amp;oq=901+Cole+&amp;sll=37.26531,-119.311523&amp;sspn=15.15959,21.401367&amp;ie=UTF8&amp;hq=&amp;hnear=901+Cole+St,+San+Francisco,+California+94117&amp;t=m&amp;ll=37.766915,-122.450352&amp;spn=0.024969,0.047464&amp;z=14&amp;output=embed"></iframe><br /><small><a href="https://maps.google.com/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=901+Cole+Street,+San+Francisco,+CA&amp;aq=0&amp;oq=901+Cole+&amp;sll=37.26531,-119.311523&amp;sspn=15.15959,21.401367&amp;ie=UTF8&amp;hq=&amp;hnear=901+Cole+St,+San+Francisco,+California+94117&amp;t=m&amp;ll=37.766915,-122.450352&amp;spn=0.024969,0.047464&amp;z=14" style="color:#0000FF;text-align:left">View Larger Map</a></small>
	    </div>
	    <!-- m_editable region-id="editable-static-tag-U821-BP_infinity" template="contact-padrecito.html" data-type="image" -->
	    <div class="clip_frame grpelem" id="u821" data-muse-uid="U821" data-muse-type="img_frame"><!-- image -->
	     	<img class="block" id="u821_img" src="<?php echo get_template_directory_uri(); ?>/images/ladama.png" alt="" width="205" height="248" data-muse-src="<?php echo get_template_directory_uri(); ?>/images/ladama.png"/>
	    </div>
	    <!-- /m_editable -->
	    <!-- m_editable region-id="editable-static-tag-U823-BP_infinity" template="contact-padrecito.html" data-type="image" -->
	    <div class="clip_frame grpelem" id="u823" data-muse-uid="U823" data-muse-type="img_frame"><!-- image -->
	     	<img class="block" id="u823_img" src="<?php echo get_template_directory_uri(); ?>/images/diablo.png" alt="" width="191" height="238" data-muse-src="<?php echo get_template_directory_uri(); ?>/images/diablo.png"/>
	    </div>
	    <!-- /m_editable -->
	    <a class="nonblock nontext grpelem" id="u1434-7" href="mailto:padre@padrecitosf.com" data-mu-ie-matrix="progid:DXImageTransform.Microsoft.Matrix(M11=0.9986,M12=-0.0523,M21=0.0523,M22=0.9986,SizingMethod='auto expand')" data-mu-ie-matrix-dx="-1" data-mu-ie-matrix-dy="-7"><!-- rasterized frame --><img id="u1434-7_img" alt="&nbsp;Padre@ PadrecitoSF.com" width="262" height="40" src="<?php echo get_template_directory_uri(); ?>/images/u1434-7.png"/></a>
	</div>
   	<div class="clearfix colelem" id="ppu808"><!-- group -->
	    <a class="nonblock nontext grpelem" id="u808" href="<?php echo home_url(); ?>" data-href="page:U65"><!-- state-based BG images --><img id="u808_states" alt="home" src="<?php echo get_template_directory_uri(); ?>/images/blank.gif"/></a>
	    <a class="nonblock nontext grpelem" id="u811" href="<?php echo home_url('menu'); ?>" data-href="page:U762"><!-- state-based BG images --><img id="u811_states" alt="menu" src="<?php echo get_template_directory_uri(); ?>/images/blank.gif"/></a>
	    <a class="nonblock nontext grpelem" id="u812" href="<?php echo home_url('about'); ?>" data-href="page:U738"><!-- state-based BG images --><img id="u812_states" alt="about" src="<?php echo get_template_directory_uri(); ?>/images/blank.gif"/></a>
	    <a class="nonblock nontext grpelem" id="u813" href="<?php echo home_url('reservations'); ?>" data-href="page:U746"><!-- state-based BG images --><img id="u813_states" alt="reservations" src="<?php echo get_template_directory_uri(); ?>/images/blank.gif"/></a>
	    <a class="nonblock nontext grpelem" id="u814" href="https://www.treatful.com/buy/Padrecito?ref=1"><!-- state-based BG images --><img id="u814_states" alt="gift certificates" src="<?php echo get_template_directory_uri(); ?>/images/blank.gif"/></a>
   	</div>
   	<div class="verticalspacer" data-offset-top="689" data-content-above-spacer="724" data-content-below-spacer="98"></div>
<?php
get_footer();
