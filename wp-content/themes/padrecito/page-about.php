<?php
/**
 * The template for displaying page about
 *
 * @author Botez Costin
 * @package Padrecito
 */

get_header(); ?>

	<div class="position_content" id="page_position_content">
    	<div class="clearfix colelem" id="pu799-4"><!-- group -->
     		<img class="grpelem" id="u799-4" alt="about" width="112" height="38" src="<?php echo get_template_directory_uri(); ?>/images/u799-4.png"/><!-- rasterized frame -->
     		<div class="clearfix grpelem" id="pu897"><!-- column -->
      			<!-- m_editable region-id="editable-static-tag-U897-BP_infinity" template="about-padrecito.html" data-type="image" data-ice-options="clickable" data-ice-editable="link" -->
      			<a class="nonblock nontext clip_frame colelem" id="u897" href="index.html" data-href="page:U65" data-muse-uid="U897" data-muse-type="img_frame"><!-- image --><img class="block" id="u897_img" src="<?php echo get_template_directory_uri(); ?>/images/padrecito_logo_blue.png" alt="" width="199" height="71" data-muse-src="<?php echo get_template_directory_uri(); ?>/images/padrecito_logo_blue.png"/></a>
      			<!-- /m_editable -->
      			<!-- m_editable region-id="editable-static-tag-U805-BP_infinity" template="about-padrecito.html" data-type="image" -->
      			<div class="clip_frame colelem" id="u805" data-muse-uid="U805" data-muse-type="img_frame"><!-- image -->
       				<img class="block" id="u805_img" src="<?php echo get_template_directory_uri(); ?>/images/padrecitoborderxtralong-crop-u805.png" alt="" width="809" height="18" data-muse-src="<?php echo get_template_directory_uri(); ?>/images/padrecitoborderxtralong-crop-u805.png"/>
      			</div>
      			<!-- /m_editable -->
     		</div>
    	</div>
    	<div class="clearfix colelem" id="pu800-13"><!-- group -->
     		<!-- m_editable region-id="editable-static-tag-U800-BP_infinity" template="about-padrecito.html" data-type="html" data-ice-options="disableImageResize,link" -->
     		<div class="clearfix grpelem" id="u800-13" data-muse-uid="U800" data-muse-type="txt_frame"><!-- content -->
      			<?php global $post; echo $post->post_content; ?>
     		</div>
     		<!-- /m_editable -->
     		<!-- m_editable region-id="editable-static-tag-U1725-BP_infinity" template="about-padrecito.html" data-type="image" -->
     		<div class="clip_frame grpelem" id="u1725" data-muse-uid="U1725" data-muse-type="img_frame"><!-- image -->
      			<img class="block" id="u1725_img" src="<?php echo get_template_directory_uri(); ?>/images/paintedbackground544x381.png" alt="" width="544" height="381" data-muse-src="<?php echo get_template_directory_uri(); ?>/images/paintedbackground544x381.png"/>
     		</div>
     		<!-- /m_editable -->
     		<!-- m_editable region-id="editable-static-tag-U1728-BP_infinity" template="about-padrecito.html" data-type="image" -->
     		<div class="clip_frame grpelem" id="u1728" data-muse-uid="U1728" data-muse-type="img_frame"><!-- image -->
      			<img class="block" id="u1728_img" src="<?php echo get_template_directory_uri(); ?>/images/dining12_padrecito_1017-600x416.jpg" alt="" width="387" height="268" data-muse-src="<?php echo get_template_directory_uri(); ?>/images/dining12_padrecito_1017-600x416.jpg"/>
     		</div>
     		<!-- /m_editable -->
    	</div>
    	<div class="clearfix colelem" id="ppu798"><!-- group -->
     		<a class="nonblock nontext grpelem" id="u798" href="<?php echo home_url(); ?>" data-href="page:U65"><!-- state-based BG images --><img id="u798_states" alt="home" src="<?php echo get_template_directory_uri(); ?>/images/blank.gif"/></a>
     		<a class="nonblock nontext grpelem" id="u801" href="<?php echo home_url('menu'); ?>" data-href="page:U762"><!-- state-based BG images --><img id="u801_states" alt="menu" src="<?php echo get_template_directory_uri(); ?>/images/blank.gif"/></a>
     		<a class="nonblock nontext grpelem" id="u802" href="<?php echo home_url('contact'); ?>" data-href="page:U770"><!-- state-based BG images --><img id="u802_states" alt="Contact" src="<?php echo get_template_directory_uri(); ?>/images/blank.gif"/></a>
     		<a class="nonblock nontext grpelem" id="u803" href="<?php echo home_url('reservations'); ?>" data-href="page:U746"><!-- state-based BG images --><img id="u803_states" alt="reservations" src="<?php echo get_template_directory_uri(); ?>/images/blank.gif"/></a>
     		<a class="nonblock nontext grpelem" id="u804" href="https://www.treatful.com/buy/Padrecito?ref=1"><!-- state-based BG images --><img id="u804_states" alt="Gift Certificates" src="<?php echo get_template_directory_uri(); ?>/images/blank.gif"/></a>
    	</div>
    	<div class="verticalspacer" data-offset-top="649" data-content-above-spacer="685" data-content-below-spacer="98"></div>
   	</div>
<?php
get_footer();
