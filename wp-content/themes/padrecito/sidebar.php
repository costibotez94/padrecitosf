<?php
/**
 * The sidebar containing the main widget area.
 *
 * @author Botez Costin
 * @package Blue_Barn
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>

<aside class='grid_3'>
	<h3>
		<span><?php _e('Bulletin Board', 'blue-barn'); ?></span>
	</h3>
  	<ul>
    	<li>
      		<p><?php _e('Please note that there is a 5 entree max for all takeout orders at all 3 Blue Barn locations (Polk, Chestnut and Marin).', 'blue-barn'); ?></p>
			<p><?php _e('Thank you for your patience and understanding.', 'blue-barn'); ?></p>
    	</li>
    	<li class='last'>&nbsp;</li>
  </ul>
</aside>
