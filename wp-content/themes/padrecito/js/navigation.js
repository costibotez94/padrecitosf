$(document).ready(function() {
  $("#home a img").hover(function(){
    $(this).attr("src", "/wp-content/themes/thetipsypig/inc/images/home-active-trans.png");
  },function(){
    $(this).attr("src", "/wp-content/themes/thetipsypig/inc/images/home-trans.png");
  });
  $("#menus a img").hover(function(){
    $(this).attr("src", "/wp-content/themes/thetipsypig/inc/images/menus-active-trans.png");
  },function(){
    $(this).attr("src", "/wp-content/themes/thetipsypig/inc/images/menus-trans.png");
  });
  $("#reservations a img").hover(function(){
    $(this).attr("src", "/wp-content/themes/thetipsypig/inc/images/reservations-active-trans.png");
  },function(){
    $(this).attr("src", "/wp-content/themes/thetipsypig/inc/images/reservations-trans.png");
  });
  $("#patio a img").hover(function(){
    $(this).attr("src", "/wp-content/themes/thetipsypig/inc/images/patio-active-trans.png");
  },function(){
    $(this).attr("src", "/wp-content/themes/thetipsypig/inc/images/patio-trans.png");
  });
  $("#shop a img").hover(function(){
    $(this).attr("src", "/wp-content/themes/thetipsypig/inc/images/shop-active-trans.png");
  },function(){
    $(this).attr("src", "/wp-content/themes/thetipsypig/inc/images/shop-trans.png");
  });
});
