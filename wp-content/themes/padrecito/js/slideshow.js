$(document).ready(function() {
  $(".tabs").tabs(".images > div", {
    effect: "fade",
    fadeOutSpeed: 200,
    rotate: true
  }).slideshow({
    autoplay: true
  });
});