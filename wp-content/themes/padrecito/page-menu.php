<?php
/**
 * The template for displaying page menu
 *
 * @author Botez Costin
 * @package Padrecito
 */

get_header(); ?>
	<div class="position_content" id="page_position_content">
    	<div class="clearfix colelem" id="pu772-4"><!-- group -->
     		<img class="grpelem" id="u772-4" alt="MENU" width="111" height="38" src="<?php echo get_template_directory_uri(); ?>/images/u772-4.png"/><!-- rasterized frame -->
     		<!-- m_editable region-id="editable-static-tag-U773-BP_infinity" template="menu-at-padrecito.html" data-type="html" data-ice-options="disableImageResize,link" -->
     		<div class="clearfix grpelem" id="u773-10" data-muse-uid="U773" data-muse-type="txt_frame"><!-- content -->
      			<?php global $post; echo $post->post_content; ?>
     		</div>
     		<!-- /m_editable -->
     		<!-- m_editable region-id="editable-static-tag-U776-BP_infinity" template="menu-at-padrecito.html" data-type="image" -->
     		<div class="clip_frame grpelem" id="u776" data-muse-uid="U776" data-muse-type="img_frame" data-mu-ie-matrix="progid:DXImageTransform.Microsoft.Matrix(M11=-1,M12=0,M21=0,M22=-1,SizingMethod='auto expand')" data-mu-ie-matrix-dx="0" data-mu-ie-matrix-dy="0"><!-- image -->
      			<img class="block" id="u776_img" src="<?php echo get_template_directory_uri(); ?>/images/paintedbackground.png" alt="" width="699" height="489" data-muse-src="<?php echo get_template_directory_uri(); ?>/images/paintedbackground.png"/>
     		</div>
     		<!-- /m_editable -->
     		<!-- m_editable region-id="editable-static-tag-U1232-BP_infinity" template="menu-at-padrecito.html" data-type="image" -->
     		<div class="clip_frame clearfix grpelem" id="u1232" data-muse-uid="U1232" data-muse-type="img_frame"><!-- image -->
      			<img class="position_content" id="u1232_img" src="<?php echo get_template_directory_uri(); ?>/images/tacoshot.jpg" alt="" width="504" height="378" data-muse-src="<?php echo get_template_directory_uri(); ?>/images/tacoshot.jpg"/>
     		</div>
 			<!-- /m_editable -->
     		<!-- m_editable region-id="editable-static-tag-U782-BP_infinity" template="menu-at-padrecito.html" data-type="image" -->
     		<div class="clip_frame grpelem" id="u782" data-muse-uid="U782" data-muse-type="img_frame"><!-- image -->
      			<img class="block" id="u782_img" src="<?php echo get_template_directory_uri(); ?>/images/diablo.png" alt="" width="191" height="238" data-muse-src="<?php echo get_template_directory_uri(); ?>/images/diablo.png"/>
     		</div>
     		<!-- /m_editable -->
     		<!-- m_editable region-id="editable-static-tag-U784-BP_infinity" template="menu-at-padrecito.html" data-type="image" -->
     		<div class="clip_frame grpelem" id="u784" data-muse-uid="U784" data-muse-type="img_frame"><!-- image -->
      			<img class="block" id="u784_img" src="<?php echo get_template_directory_uri(); ?>/images/ladama.png" alt="" width="205" height="248" data-muse-src="<?php echo get_template_directory_uri(); ?>/images/ladama.png"/>
     		</div>
     		<!-- /m_editable -->
     		<a class="nonblock nontext grpelem" id="u786-4" href="<?php echo get_template_directory_uri(); ?>/inc/assets/padre.dinner.fall.10.04.16.pdf" data-href="upload:U2285" data-mu-ie-matrix="progid:DXImageTransform.Microsoft.Matrix(M11=0.9994,M12=0.0349,M21=-0.0349,M22=0.9994,SizingMethod='auto expand')" data-mu-ie-matrix-dx="0" data-mu-ie-matrix-dy="-3"><!-- rasterized frame --><img id="u786-4_img" alt="Dinner Menu" width="198" height="22" src="<?php echo get_template_directory_uri(); ?>/images/u786-4.png"/></a>
     		<a class="nonblock nontext grpelem" id="u1521-4" href="<?php echo get_template_directory_uri(); ?>/inc/assets/padre.mezcal.08.10.16.pdf" data-href="upload:U2293" data-mu-ie-matrix="progid:DXImageTransform.Microsoft.Matrix(M11=0.9994,M12=0.0349,M21=-0.0349,M22=0.9994,SizingMethod='auto expand')" data-mu-ie-matrix-dx="0" data-mu-ie-matrix-dy="-2"><!-- rasterized frame --><img id="u1521-4_img" alt="tequila" width="122" height="22" src="<?php echo get_template_directory_uri(); ?>/images/u1521-4.png"/></a>
    		<a class="nonblock nontext grpelem" id="u1522-4" href="<?php echo get_template_directory_uri(); ?>/inc/assets/padre.dinner-cocktail.cover.07.10.162.pdf" data-href="upload:U2295" data-mu-ie-matrix="progid:DXImageTransform.Microsoft.Matrix(M11=0.9998,M12=-0.0175,M21=0.0175,M22=0.9998,SizingMethod='auto expand')" data-mu-ie-matrix-dx="0" data-mu-ie-matrix-dy="-2"><!-- rasterized frame --><img id="u1522-4_img" alt="beer &amp; Wine" width="190" height="22" src="<?php echo get_template_directory_uri(); ?>/images/u1522-4.png"/></a>
     		<a class="nonblock nontext grpelem" id="u787-4" href="<?php echo get_template_directory_uri(); ?>/inc/assets/padre.dinner-cocktails.08.03.16.pdf" data-href="upload:U2289" data-mu-ie-matrix="progid:DXImageTransform.Microsoft.Matrix(M11=0.9994,M12=-0.0349,M21=0.0349,M22=0.9994,SizingMethod='auto expand')" data-mu-ie-matrix-dx="0" data-mu-ie-matrix-dy="-3"><!-- rasterized frame --><img id="u787-4_img" alt="cocktails" width="162" height="22" src="<?php echo get_template_directory_uri(); ?>/images/u787-4.png"/></a>
     		<a class="nonblock nontext grpelem" id="u1743-4" href="<?php echo get_template_directory_uri(); ?>/inc/assets/padre.brunch.fall.09.30.16.pdf" data-href="upload:U2287"><!-- rasterized frame --><img id="u1743-4_img" alt="BRUNCH" width="124" height="22" src="<?php echo get_template_directory_uri(); ?>/images/u1743-4.png"/></a>
     		<a class="nonblock nontext grpelem" id="u1742-6" href="<?php echo get_template_directory_uri(); ?>/inc/assets/padre.brunch-cocktails.01.15.162.pdf" data-href="upload:U2291" data-mu-ie-matrix="progid:DXImageTransform.Microsoft.Matrix(M11=0.9994,M12=-0.0349,M21=0.0349,M22=0.9994,SizingMethod='auto expand')" data-mu-ie-matrix-dx="-1" data-mu-ie-matrix-dy="-3"><!-- rasterized frame --><img id="u1742-6_img" alt="BRUNCH cocktails" width="161" height="41" src="<?php echo get_template_directory_uri(); ?>/images/u1742-6.png"/></a>
     		<!-- m_editable region-id="editable-static-tag-U790-BP_infinity" template="menu-at-padrecito.html" data-type="image" -->
     		<div class="clip_frame grpelem" id="u790" data-muse-uid="U790" data-muse-type="img_frame"><!-- image -->
      			<img class="block" id="u790_img" src="<?php echo get_template_directory_uri(); ?>/images/padrecitoborderxtralong-crop-u790.png" alt="" width="835" height="18" data-muse-src="<?php echo get_template_directory_uri(); ?>/images/padrecitoborderxtralong-crop-u790.png"/>
     		</div>
     		<!-- /m_editable -->
     		<!-- m_editable region-id="editable-static-tag-U909-BP_infinity" template="menu-at-padrecito.html" data-type="image" data-ice-options="clickable" data-ice-editable="link" -->
     		<a class="nonblock nontext clip_frame grpelem" id="u909" href="<?php echo home_url(); ?>" data-href="page:U65" data-muse-uid="U909" data-muse-type="img_frame"><!-- image --><img class="block" id="u909_img" src="<?php echo get_template_directory_uri(); ?>/images/padrecito_logo_blue.png" alt="" width="199" height="71" data-muse-src="<?php echo get_template_directory_uri(); ?>/images/padrecito_logo_blue.png"/></a>
     		<!-- /m_editable -->
    	</div>
    	<div class="clearfix colelem" id="pu1885-4"><!-- group -->
     		<a class="nonblock nontext grpelem" id="u1885-4" href="<?php echo get_template_directory_uri(); ?>/inc/assets/changeable-kids-menu-(on-illustrator).pdf" data-href="upload:U1887" data-mu-ie-matrix="progid:DXImageTransform.Microsoft.Matrix(M11=0.9998,M12=-0.0175,M21=0.0175,M22=0.9998,SizingMethod='auto expand')" data-mu-ie-matrix-dx="0" data-mu-ie-matrix-dy="-1"><!-- rasterized frame --><img id="u1885-4_img" alt="KIDS" width="76" height="22" src="<?php echo get_template_directory_uri(); ?>/images/u1885-4.png"/></a>
     		<a class="nonblock nontext grpelem" id="u1548-4" href="<?php echo get_template_directory_uri(); ?>/inc/assets/padre.dessert.cervesas-05.20.16.pdf" data-href="upload:U2283" data-mu-ie-matrix="progid:DXImageTransform.Microsoft.Matrix(M11=0.9998,M12=-0.0175,M21=0.0175,M22=0.9998,SizingMethod='auto expand')" data-mu-ie-matrix-dx="0" data-mu-ie-matrix-dy="-1"><!-- rasterized frame --><img id="u1548-4_img" alt="dessert" width="125" height="22" src="<?php echo get_template_directory_uri(); ?>/images/u1548-4.png"/></a>
    	</div>
    	<div class="clearfix colelem" id="ppu771"><!-- group -->
     		<a class="nonblock nontext grpelem" id="u771" href="<?php echo home_url(); ?>" data-href="page:U65"><!-- state-based BG images --><img id="u771_states" alt="home" src="<?php echo get_template_directory_uri(); ?>/images/blank.gif"/></a>
     		<a class="nonblock nontext grpelem" id="u774" href="<?php echo home_url('about'); ?>" data-href="page:U738"><!-- state-based BG images --><img id="u774_states" alt="about" src="<?php echo get_template_directory_uri(); ?>/images/blank.gif"/></a>
     		<a class="nonblock nontext grpelem" id="u775" href="<?php echo home_url('contact'); ?>" data-href="page:U770"><!-- state-based BG images --><img id="u775_states" alt="Contact" src="<?php echo get_template_directory_uri(); ?>/images/blank.gif"/></a>
     		<a class="nonblock nontext grpelem" id="u778" href="<?php echo home_url('reservations'); ?>" data-href="page:U746"><!-- state-based BG images --><img id="u778_states" alt="reservations" src="<?php echo get_template_directory_uri(); ?>/images/blank.gif"/></a>
     		<a class="nonblock nontext grpelem" id="u779" href="https://www.treatful.com/buy/Padrecito?ref=1"><!-- state-based BG images --><img id="u779_states" alt="Gift certificates" src="<?php echo get_template_directory_uri(); ?>/images/blank.gif"/></a>
    	</div>
    <div class="verticalspacer" data-offset-top="694" data-content-above-spacer="750" data-content-below-spacer="62"></div>
   </div>
<?php
get_footer();
