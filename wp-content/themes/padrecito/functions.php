<?php
/**
 * Blue Barn functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Padrecito
 */

if ( ! function_exists( 'padrecito_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function padrecito_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Blue Barn, use a find and replace
	 * to change 'padrecito' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'padrecito', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'padrecito' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	// add_theme_support( 'custom-background', apply_filters( 'padrecito_custom_background_args', array(
	// 	'default-color' => 'ffffff',
	// 	'default-image' => '',
	// ) ) );
}
endif;
add_action( 'after_setup_theme', 'padrecito_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function padrecito_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'padrecito_content_width', 640 );
}
add_action( 'after_setup_theme', 'padrecito_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function padrecito_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'padrecito' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'padrecito' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
// add_action( 'widgets_init', 'padrecito_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function padrecito_scripts() {
	//wp_enqueue_script( 'padrecito-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	wp_enqueue_style( 'padrecito-about-padrecito', get_template_directory_uri() . '/css/about-padrecito.css' );
	wp_enqueue_style( 'padrecito-contact-padrecito', get_template_directory_uri() . '/css/contact-padrecito.css' );
	wp_enqueue_style( 'padrecito-menu', get_template_directory_uri() . '/css/menu-at-padrecito.css' );
	wp_enqueue_style( 'padrecito-reservations', get_template_directory_uri() . '/css/reservations-at-padrecito.css' );
	wp_enqueue_style( 'padrecito-global', get_template_directory_uri() . '/css/site_global.css' );
	wp_enqueue_style( 'pagesheet', get_template_directory_uri() . '/css/index.css');


	wp_enqueue_script( 'padrecito-jquery-main', get_template_directory_uri() . '/js/jquery-1.8.3.min.js');
	// wp_enqueue_script( 'padrecito-jquery-musepolyfill', get_template_directory_uri() . '/js/jquery.musepolyfill.bgsize.js');
	// wp_enqueue_script( 'padrecito-jquery-watch', get_template_directory_uri() . '/js/jquery.watch.js');
	// wp_enqueue_script( 'padrecito-museconfig', get_template_directory_uri() . '/js/museconfig.js');
	// wp_enqueue_script( 'padrecito-museredirect', get_template_directory_uri() . '/js/museredirect.js');
	// wp_enqueue_script( 'padrecito-museutils', get_template_directory_uri() . '/js/museutils.js');
	// wp_enqueue_script( 'padrecito-require', get_template_directory_uri() . '/js/require.js');
	// wp_enqueue_script( 'padrecito-whatinput', get_template_directory_uri() . '/js/whatinput.js');

	// wp_enqueue_style( 'padrecito-handheld', get_template_directory_uri() . '/css/handheld.css' );
}
add_action( 'wp_enqueue_scripts', 'padrecito_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Menus Custom Post Type
 * @author Botez Costin
 */
function menus_custom_post_type() {

	// Set UI labels for Custom Post Type
	$labels = array(
		'name'                => _x( 'Menus', 'Post Type General Name', 'padrecito' ),
		'singular_name'       => _x( 'Menu', 'Post Type Singular Name', 'padrecito' ),
		'menu_name'           => __( 'Menus', 'padrecito' ),
		'parent_item_colon'   => __( 'Parent Menu', 'padrecito' ),
		'all_items'           => __( 'All Menus', 'padrecito' ),
		'view_item'           => __( 'View Menu', 'padrecito' ),
		'add_new_item'        => __( 'Add New Menu', 'padrecito' ),
		'add_new'             => __( 'Add New', 'padrecito' ),
		'edit_item'           => __( 'Edit Menu', 'padrecito' ),
		'update_item'         => __( 'Update Menu', 'padrecito' ),
		'search_items'        => __( 'Search Menu', 'padrecito' ),
		'not_found'           => __( 'Not Found', 'padrecito' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'padrecito' ),
	);

	// Set other options for Custom Post Type

	$args = array(
		'label'               => __( 'menus', 'padrecito' ),
		'description'         => __( 'Menus news and reviews', 'padrecito' ),
		'labels'              => $labels,
		// Features this CPT supports in Post Editor
		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
		/* A hierarchical CPT is like Pages and can have
		* Parent and child items. A non-hierarchical CPT
		* is like Posts.
		*/
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);

	// Registering your Custom Post Type
	register_post_type( 'menu', $args );
}
// add_action( 'init', 'menus_custom_post_type', 0 );

/**
 * 	Updated messages
 *	@author Botez Costin
 *	@version 1.0
 */
function my_updated_messages( $messages ) {
  global $post, $post_ID;
  $messages['menu'] = array(
    0 => '',
    1 => sprintf( __('Menus updated. <a href="%s">View menu</a>'), esc_url( get_permalink($post_ID) ) ),
    2 => __('Custom field updated.'),
    3 => __('Custom field deleted.'),
    4 => __('Product updated.'),
    5 => isset($_GET['revision']) ? sprintf( __('Menu restored to revision from %s'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
    6 => sprintf( __('Menu published. <a href="%s">View menu</a>'), esc_url( get_permalink($post_ID) ) ),
    7 => __('Menu saved.'),
    8 => sprintf( __('Menu submitted. <a target="_blank" href="%s">Preview menu</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
    9 => sprintf( __('Menu scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview menu</a>'), date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
    10 => sprintf( __('Menu draft updated. <a target="_blank" href="%s">Preview menu</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
  );
  return $messages;
}
// add_filter( 'post_updated_messages', 'my_updated_messages' );

/**
 * 	Create custom pages
 *	@author Botez Costin
 *	@version 1.0
 */
function create_custom_pages() {
	// Create custom pages

	// HOME page
	$post = get_page_by_title('Home');
	if($post == NULL || $post->post_status == 'trash') {

		$my_post = array(
		  'post_title'    => 'Home',
		  'post_status'   => 'publish',
		  'post_author'   => 1,
		  'post_type'	  => 'page'
		);

		// Insert the post into the database
		wp_insert_post( $my_post );
	}

	// ABOUT page
	$post = get_page_by_title('About');
	if($post == NULL || $post->post_status == 'trash') {

		$about_content  = '<p>Padrecito is a seasonal, and we think sensational mexican restaurant in the heart of San Francisco’s Cole Valley neighborhood.&nbsp; At Padrecito we strive to create a community atmosphere while serving prideful culinary creations and libations utilizing seasonal ingredients.&nbsp; Many of these ingredients come from our sister farm in Sonoma, Oak Hill Farm.&nbsp; At the core of our offering is a homage to original Mexican dishes made from scratch each day.&nbsp; House made tortillas and salsas create the foundation for our ever seasonal menu. While unique and carefully crafted ceviches, enchiladas and tacos have become our house staples.</p>';
		$about_content .= '<p>&nbsp;</p>';
		$about_content .= '<p>Our tequila, mezcal and bar program match the thoughtfulness and seasonality of our culinary offering.&nbsp; We search out hand crafted and boutique distilleries to round out our over 80 in house tequilas and mezcals.&nbsp; These agave based spirits create the base of our cocktail menu while bourbon, rum and other spirits make a tasteful appearance.</p>';
		$about_content .= '<p>&nbsp;</p>';
		$about_content .= '<p>We hope to see you here and hope you enjoy what we enjoy creating everyday.</p>';
		$about_content .= '<p>&nbsp;</p>';
		$about_content .= '<p>The Padrecito Family</p>';

		$my_post = array(
		  'post_title'    => 'About',
		  'post_status'   => 'publish',
		  'post_author'   => 1,
		  'post_type'	  => 'page',
		  'post_content'   => $about_content
		);

		// Insert the post into the database
		wp_insert_post( $my_post );
	}

	// MENU page
	$post = get_page_by_title('Menu');
	if($post == NULL || $post->post_status == 'trash') {

    	$menu_content = '<p>We are proud to offer only “never-ever” meats &amp; poultry from Mary’s, Marin Sun Farms, Niman and Meyer Ranch; as well as seafood approved by the Monterey Bay Aquarium watch list. We also strive to use organic produce whenever possible and are very dearly inspired by the seasonal and sustainable harvests from Oak Hill Farm, located locally here in Sonoma.</p>';
    	$menu_content .= '<p>&nbsp;</p>';
		$menu_content .= '<p>Our cocktails have been carefully crafted to pair and walk in stride with our culinary offering and philopshy.</p>';
    	$menu_content .= '<p>&nbsp;</p>';
    	$menu_content .= '<p>Please click the links below to view our current offerings:</p>';

		$my_post = array(
		  'post_title'    => 'Menu',
		  'post_status'   => 'publish',
		  'post_author'   => 1,
		  'post_type'	  => 'page',
		  'post_content'  => $menu_content,
		);

		// Insert the post into the database
		wp_insert_post( $my_post );
	}

	// RESERVATIONS page
	$post = get_page_by_title('Reservations');
	if($post == NULL || $post->post_status == 'trash') {
		$reservation_content  = '<p>Please use the above tabs to make an online reservation through Opentable.</p>';
		$reservation_content .= '<p>&nbsp;</p>';
        $reservation_content .= '<p>We only have a limited number of reservations available as we save tables for walk-ins.</p>';
		$reservation_content .= '<p>&nbsp;</p>';
      	$reservation_content .= '<p>If you have any special requests or inquiries, please call us at</p>';
      	$reservation_content .= '<p>415.742.5505</p>';

		$my_post = array(
		  'post_title'    => 'Reservations',
		  'post_status'   => 'publish',
		  'post_author'   => 1,
		  'post_type'	  => 'page',
		  'post_content'  => $reservation_content
		);

		// Insert the post into the database
		wp_insert_post( $my_post );
	}

	// CONTACT page
	$post = get_page_by_title('Contact');
	if($post == NULL || $post->post_status == 'trash') {

		$contact_content  = '<p>Padrecito is located in the heart of San Francisco\'s classic Cole Valley neighborhood.</p>';
		$contact_content .= '<p>&nbsp;</p>';
		$contact_content .= '<p>For private party requests or general questions and inquires, please call 415.742.5505</p>';
     	$contact_content .= '<p>&nbsp;</p>';
     	$contact_content .= '<p>Please note that there is a parking lot located 2 blocks away on Stanyan between Frederick and Waller</p>';

		$my_post = array(
		  'post_title'    => 'Contact',
		  'post_status'   => 'publish',
		  'post_author'   => 1,
		  'post_type'	  => 'page',
		  'post_content'  => $contact_content
		);

		// Insert the post into the database
		wp_insert_post( $my_post );
	}
}
add_action('init', 'create_custom_pages');

function padrecito_insert_cpt_posts() {
	// LOCATIONS
	$location_1 = get_page_by_title('BLUE BARN POLK ST', OBJECT, 'location');
	if($location_1 == NULL || $location_1->post_status == 'trash') {
		$location_1_content  = '<p>HOURS 11AM-8:30PM MONDAY-FRIDAY, &amp; SATURDAY &amp; SUNDAY 11AM-8PM</p>';
		$location_1_content .= '<p>WE ARE NOW TAKING PHONE ORDERS WITH A LIMIT OF 5 ITEMS FOR ALL PICK-UP OR TAKE-OUT ORDERS.</p>';
		$location_1_content .= '<p>415.655.9438</p>';
		$location_1_excerpt = '<p>2237 POLK ST<br>SAN FRANCISCO, CA 94109</p>';

		$my_post = array(
		  'post_title'    => 'BLUE BARN POLK ST',
		  'post_status'   => 'publish',
		  'post_author'   => 1,
		  'post_type'	  => 'location',
		  'post_content'  => $location_1_content,
		  'post_excerpt'  => $location_1_excerpt
		);

		// Insert the post into the database
		$location_ID = wp_insert_post( $my_post );
		$custom_URLs = '/static/media/uploads/menus/BlueBarnSpring2016.pdf-Chestnut Salad & Sando Menu|';
		$custom_URLs .= '/static/media/uploads/menus/BlueMaster.Upick.SPRING.2.1.16.pdf-Chestnut U-Pick Salad Menu';
		update_post_meta($location_ID, 'location_menus', $custom_URLs);
	}

	$location_1 = get_page_by_title('BLUE BARN MARIN', OBJECT, 'location');
	if($location_1 == NULL || $location_1->post_status == 'trash') {
		$location_1_content  = '<p>HOURS:  11:00AM-8:00PM DAILY</p>';
		$location_1_content .= '<p>PLEASE NOTE AS MARIN HAS A LIMITED CAPACITY WE CAN ONLY ACCEPT ORDERS OF FIVE (5) ITEMS MAXIMUM FOR CARRY-OUT, OR PHONE PICK-UP. </p>';
		$location_1_content .= '<p>415.655.9438</p>';
		$location_1_excerpt = '<p>335 CORTE MADERA TOWN CENTER<br>CORTE MADERA , CA 94925<br>415-927-1104</p>';

		$my_post = array(
		  'post_title'    => 'BLUE BARN MARIN',
		  'post_status'   => 'publish',
		  'post_author'   => 1,
		  'post_type'	  => 'location',
		  'post_content'  => $location_1_content,
		  'post_excerpt'  => $location_1_excerpt
		);

		// Insert the post into the database
		$location_ID = wp_insert_post( $my_post );
		$custom_URLs = '/static/media/uploads/menus/BlueBarnSpring2016.pdf-Marin Salads & Sandos Menu|';
		$custom_URLs .= '/static/media/uploads/menus/BlueMaster.Upick.SPRING.2.1.16.pdf-Marin U-Pick Salad Menu';
		update_post_meta($location_ID, 'location_menus', $custom_URLs);
	}

	$location_1 = get_page_by_title('BLUE BARN CHESTNUT', OBJECT, 'location');
	if($location_1 == NULL || $location_1->post_status == 'trash') {
		$location_1_content  = '<p>HOURS: MONDAY - THURSDAY 11:00AM - 8:30PM FRIDAY - SUNDAY 11:00AM - 8:00PM</p>';
		$location_1_content .= '<p>PLEASE NOTE AS CHESTNUT HAS A LIMITED CAPACITY WE CAN ONLY ACCEPT ORDERS OF FIVE (5) ITEMS MAXIMUM FOR CARRY-OUT, OR PHONE PICK-UP. </p>';
		$location_1_content .= '<p>415.655.9438</p>';
		$location_1_excerpt = '<p>2105 CHESTNUT STREET<br>SAN FRANCISCO, CA 94123<br>415-441-3232</p>';

		$my_post = array(
		  'post_title'    => 'BLUE BARN CHESTNUT',
		  'post_status'   => 'publish',
		  'post_author'   => 1,
		  'post_type'	  => 'location',
		  'post_content'  => $location_1_content,
		  'post_excerpt'  => $location_1_excerpt
		);

		// Insert the post into the database
		$location_ID = wp_insert_post( $my_post );
		$custom_URLs = '/static/media/uploads/menus/BlueBarnSpring2016.pdf-Chestnut Salad & Sando Menu|';
		$custom_URLs .= '/static/media/uploads/menus/BlueMaster.Upick.SPRING.2.1.16.pdf-Chestnut U-Pick Salad Menu';
		update_post_meta($location_ID, 'location_menus', $custom_URLs);
	}

	// MENUS
	$menus_1 = get_page_by_title('Blue Barn Polk St', OBJECT, 'menu');
	if($menus_1 == NULL || $menus_1->post_status == 'trash') {
		$menus_1_content  = '<ul>';
		$menus_1_content .= '	<li><a href="' . get_template_directory_uri(). '/static/media/uploads/menus/BlueMaster.Inside.SPRING.2016.pdf">Polk Salad &amp; Sando Menu</a></li>';
		$menus_1_content .='<li><a href="' . get_template_directory_uri(). '/static/media/uploads/menus/BlueMaster.Upick.SPRING.2.1.16.pdf">Polk U-Pick Salad</a></li></ul>';

		$my_post = array(
		  'post_title'    => 'Blue Barn Polk St',
		  'post_status'   => 'publish',
		  'post_author'   => 1,
		  'post_type'	  => 'menu',
		  'post_content'  => $menus_1_content
		);
		wp_insert_post( $my_post );
	}

	$menus_1 = get_page_by_title('Blue Barn Marin', OBJECT, 'menu');
	if($menus_1 == NULL || $menus_1->post_status == 'trash') {
		$menus_1_content  = '<ul>';
		$menus_1_content .= '	<li><a href="' . get_template_directory_uri(). '/static/media/uploads/menus/BlueBarnSpring2016.pdf">Marin Salads &amp; Sandos Menu</a></li>';
		$menus_1_content .='<li><a href="' . get_template_directory_uri(). '/static/media/uploads/menus/BlueMaster.Upick.SPRING.2.1.16.pdf">Marin U-Pick Salad Menu</a></li></ul>';

		$my_post = array(
		  'post_title'    => 'Blue Barn Marin',
		  'post_status'   => 'publish',
		  'post_author'   => 1,
		  'post_type'	  => 'menu',
		  'post_content'  => $menus_1_content
		);
		wp_insert_post( $my_post );
	}

	$menus_1 = get_page_by_title('Blue Barn Chestnut', OBJECT, 'menu');
	if($menus_1 == NULL || $menus_1->post_status == 'trash') {
		$menus_1_content  = '<ul>';
		$menus_1_content .= '	<li><a href="' . get_template_directory_uri(). '/static/media/uploads/menus/BlueBarnSpring2016.pdf">Chestnut Salad & Sando Menu</a></li>';
		$menus_1_content .='<li><a href="' . get_template_directory_uri(). '/static/media/uploads/menus/BlueMaster.Upick.SPRING.2.1.16.pdf">Chestnut U-Pick Salad Menu</a></li></ul>';

		$my_post = array(
		  'post_title'    => 'Blue Barn Chestnut',
		  'post_status'   => 'publish',
		  'post_author'   => 1,
		  'post_type'	  => 'menu',
		  'post_content'  => $menus_1_content
		);
		wp_insert_post( $my_post );
	}
}
// add_action('init', 'padrecito_insert_cpt_posts');
/**
 *	CHANGE FRONT PAGE
 *	@author Botez Costin
 */
function switch_homepage() {
    $page = get_page_by_title( 'Home' );
    update_option( 'page_on_front', $page->ID );
    update_option( 'show_on_front', 'page' );
}
add_action( 'init', 'switch_homepage' );

/**
 * Menus Metabox
 * @author Botez Costin
 * @version 1.0
 */
function menus_init() {
	global $post;
	if(get_the_title($post->ID) == 'Menu')
		add_meta_box('menus-metabox', 'Menu Links Box', 'menus_cb', 'page', 'side', 'high');
}
add_action( 'add_meta_boxes', 'menus_init' );

/**
 * Back end HTML
 * @author Botez Costin
 * @version 1.0
 */
function menus_cb( $post ) {

	$menus_links = get_post_meta($_GET['post'], 'menus_links' , true ); ?>
  	<p>
  		<textarea name="menus_links" id="menus_links"><?php echo $menus_links; ?></textarea>
	</p>
  	<?php
}

/**
 * Save action
 * @author Botez Costin
 * @version 1.0
 */
function save_menus_links( $post_id ) {
  	if( !empty($_POST['menus_links']) )
    	update_post_meta($post_id, 'menus_links', $_POST['menus_links'] );
}
add_action( 'save_post', 'save_menus_links', 10, 1);